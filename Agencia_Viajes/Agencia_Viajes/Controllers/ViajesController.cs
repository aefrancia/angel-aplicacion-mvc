﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agencia_Viajes.Models;

namespace Agencia_Viajes.Controllers
{
    public class ViajesController : Controller
    {
        private AgenciaDB db = new AgenciaDB();

        //
        // GET: /Viajes/

        public ActionResult Index()
        {
            return View(db.Viajes.ToList());
        }

        //
        // GET: /Viajes/Details/5

        public ActionResult Details(int ID_Viaje)
        {
            Viajes viajes = db.Viajes.Find(ID_Viaje);
            if (viajes == null)
            {
                return HttpNotFound();
            }
            return View(viajes);
        }

        //
        // GET: /Viajes/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Viajes/Create

        [HttpPost]
        public ActionResult Create(Viajes viajes)
        {
            if (ModelState.IsValid)
            {
                db.Viajes.Add(viajes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(viajes);
        }

        //
        // GET: /Viajes/Edit/5

        public ActionResult Edit(int ID_Viaje)
        {
            Viajes viajes = db.Viajes.Find(ID_Viaje);
            if (viajes == null)
            {
                return HttpNotFound();
            }
            return View(viajes);
        }

        //
        // POST: /Viajes/Edit/5

        [HttpPost]
        public ActionResult Edit(Viajes viajes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viajes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viajes);
        }

        //
        // GET: /Viajes/Delete/5

        public ActionResult Delete(int ID_Viaje)
        {
            Viajes viajes = db.Viajes.Find(ID_Viaje);
            if (viajes == null)
            {
                return HttpNotFound();
            }
            return View(viajes);
        }

        //
        // POST: /Viajes/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int ID_Viaje)
        {
            Viajes viajes = db.Viajes.Find(ID_Viaje);
            db.Viajes.Remove(viajes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}