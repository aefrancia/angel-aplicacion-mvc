﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agencia_Viajes.Models;

namespace Agencia_Viajes.Controllers
{
    public class ViajeroController : Controller
    {
        private AgenciaDB db = new AgenciaDB();

        //
        // GET: /Viajero/

        public ActionResult Index()
        {
            return View(db.Viajero.ToList());
        }

        //
        // GET: /Viajero/Details/5

        public ActionResult Details(int ID_Viajero)
        {
            Viajero viajero = db.Viajero.Find(ID_Viajero);
            if (viajero == null)
            {
                return HttpNotFound();
            }
            return View(viajero);
        }

        //
        // GET: /Viajero/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Viajero/Create

        [HttpPost]
        public ActionResult Create(Viajero viajero)
        {
            if (ModelState.IsValid)
            {
                db.Viajero.Add(viajero);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(viajero);
        }

        //
        // GET: /Viajero/Edit/5

        public ActionResult Edit(int ID_Viajero)
        {
            Viajero viajero = db.Viajero.Find(ID_Viajero);
            if (viajero == null)
            {
                return HttpNotFound();
            }
            return View(viajero);
        }

        //
        // POST: /Viajero/Edit/5

        [HttpPost]
        public ActionResult Edit(Viajero viajero)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viajero).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viajero);
        }

        //
        // GET: /Viajero/Delete/5

        public ActionResult Delete(int ID_Viajero)
        {
            Viajero viajero = db.Viajero.Find(ID_Viajero);
            if (viajero == null)
            {
                return HttpNotFound();
            }
            return View(viajero);
        }

        //
        // POST: /Viajero/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int ID_Viajero)
        {
            Viajero viajero = db.Viajero.Find(ID_Viajero);
            db.Viajero.Remove(viajero);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult AsignarViajero(int ID_Viajero)
        {
            Viajero viajero = db.Viajero.Find(ID_Viajero);
            if (viajero == null)
            {
                return HttpNotFound();
            }

            var vi = (from v in db.Viajes
                      select v);

            ViewBag.ID_Viajero = ID_Viajero;
            ViewBag.ID_Viajes = new SelectList(vi, "ID_Viajes", "destino");
            return View(viajero);
        }

        [HttpPost]
        public ActionResult AsignarViajero(int ID_Viajero, int ID_Viajes)
        {

            Viaje_V Viaje = new Viaje_V();

            Viaje.ID_Viajero = ID_Viajero;
            Viaje.ID_Viajes = ID_Viajes;
            
            db.Viaje_V.Add(Viaje);
            db.SaveChanges();

            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}