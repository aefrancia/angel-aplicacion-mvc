﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agencia_Viajes.Models;

namespace Agencia_Viajes.Controllers
{
    public class HomeController : Controller
    {
        public AgenciaDB db = new AgenciaDB();

        public ActionResult Index()
        {
            var viajes = (from v in db.Viajes
                          select v).ToList();

            return View(viajes);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Página de descripción de la aplicación.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Página de contacto.";

            return View();
        }
    }
}
