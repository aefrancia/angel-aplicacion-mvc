﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agencia_Viajes.Models;

namespace Agencia_Viajes.Controllers
{
    public class Viaje_VController : Controller
    {
        private AgenciaDB db = new AgenciaDB();

        //
        // GET: /Viaje_V/

        public ActionResult Index()
        {
            var viaje_v = db.Viaje_V.Include(v => v.Viajero).Include(v => v.Viajes);
            return View(viaje_v.ToList());
        }

        //
        // GET: /Viaje_V/Details/5

        public ActionResult Details(int id = 0)
        {
            Viaje_V viaje_v = db.Viaje_V.Find(id);
            if (viaje_v == null)
            {
                return HttpNotFound();
            }
            return View(viaje_v);
        }

        //
        // GET: /Viaje_V/Create

        public ActionResult Create()
        {
            ViewBag.ID_Viajero = new SelectList(db.Viajero, "ID_Viajero", "CI");
            ViewBag.ID_Viajes = new SelectList(db.Viajes, "ID_Viajes", "codigo_viajes");
            return View();
        }

        //
        // POST: /Viaje_V/Create

        [HttpPost]
        public ActionResult Create(Viaje_V viaje_v)
        {
            if (ModelState.IsValid)
            {
                db.Viaje_V.Add(viaje_v);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_Viajero = new SelectList(db.Viajero, "ID_Viajero", "CI", viaje_v.ID_Viajero);
            ViewBag.ID_Viajes = new SelectList(db.Viajes, "ID_Viajes", "codigo_viajes", viaje_v.ID_Viajes);
            return View(viaje_v);
        }

        //
        // GET: /Viaje_V/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Viaje_V viaje_v = db.Viaje_V.Find(id);
            if (viaje_v == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Viajero = new SelectList(db.Viajero, "ID_Viajero", "CI", viaje_v.ID_Viajero);
            ViewBag.ID_Viajes = new SelectList(db.Viajes, "ID_Viajes", "codigo_viajes", viaje_v.ID_Viajes);
            return View(viaje_v);
        }

        //
        // POST: /Viaje_V/Edit/5

        [HttpPost]
        public ActionResult Edit(Viaje_V viaje_v)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viaje_v).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_Viajero = new SelectList(db.Viajero, "ID_Viajero", "CI", viaje_v.ID_Viajero);
            ViewBag.ID_Viajes = new SelectList(db.Viajes, "ID_Viajes", "codigo_viajes", viaje_v.ID_Viajes);
            return View(viaje_v);
        }

        //
        // GET: /Viaje_V/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Viaje_V viaje_v = db.Viaje_V.Find(id);
            if (viaje_v == null)
            {
                return HttpNotFound();
            }
            return View(viaje_v);
        }

        //
        // POST: /Viaje_V/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Viaje_V viaje_v = db.Viaje_V.Find(id);
            db.Viaje_V.Remove(viaje_v);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}