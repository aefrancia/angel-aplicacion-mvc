﻿$(document).ready(function () {

});


function popupDiv(div, titleDiv, w, h) {
    var wi = w + "vw";
    var hi = h + "vw";
    div.dialog({ autoOpen: false,
        width: wi,
        title: titleDiv,
        modal: true,
        resizable: false,
        stack: true,
        draggable: false,
        closeOnEscape: false,
        open: function (event, ui) { jQuery('.ui-dialog-titlebar-close').hide(); },
        dialogClass: 'hide-close',
        position: { my: 'center', at: 'top+150' }

    });
}

$(".ui-dialog-titlebar-close").hide();

function dialog_alert(Mensaje) {

    $("#DialogoPopUp_alert").html('<p>' + Mensaje + '<p>');
    $("#DialogoPopUp_alert").dialog(
    {

        modal: true,
        resizable: false,
        stack: true,
        draggable: false,
        dialogClass: 'hide-close',
        closeOnEscape: false
    });
}

function dialog_alert_Error(Mensaje) {

    $("#DialogoPopUp_alert").html('<p>' + Mensaje + '<p>');
    $("#DialogoPopUp_alert").dialog(
    {

        modal: true,
        resizable: false,
        stack: true,
        draggable: false,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            Ok: function () {
                $(this).empty();
                $(this).removeAttr('style');
                $(this).dialog("destroy");

            }
        }
    });
}


/*****************************************************************************************/
/*                              FUNCION CERRAR                                           */
/*****************************************************************************************/
function Cerrar() {

    $("#LevantarPopup").empty();
    $("#LevantarPopup").removeAttr('style');
    $("#LevantarPopup").dialog("destroy");
}

/***********************************************************************************************************************/
/**************                                     Viajes                                             ****************/
/***********************************************************************************************************************/
function CrearViaje() {

    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajes/Create');
    popupDiv(div, "", 25, 470);
    div.dialog('open');
}

function EditViaje(ID_Viaje) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajes/Edit?ID_Viaje=' + ID_Viaje);
    popupDiv(div, "", 25, 540);
    div.dialog('open');
}

function EliminarViaje(ID_Viaje) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajes/Delete?ID_Viaje=' + ID_Viaje);
    popupDiv(div, "", 25, 430);
    div.dialog('open');
}

function VerViaje(ID_Viaje) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajes/Details?ID_Viaje=' + ID_Viaje);
    popupDiv(div, "", 25, 430);
    div.dialog('open');
}


/***********************************************************************************************************************/
/**************                                     Viajero                                             ****************/
/***********************************************************************************************************************/
function CrearViajero() {

    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajero/Create');
    popupDiv(div, "", 25, 470);
    div.dialog('open');
}

function EditViajero(ID_Viajero) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajero/Edit?ID_Viajero=' + ID_Viajero);
    popupDiv(div, "", 25, 540);
    div.dialog('open');
}

function EliminarViajero(ID_Viajero) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajero/Delete?ID_Viajero=' + ID_Viajero);
    popupDiv(div, "", 25, 430);
    div.dialog('open');
}

function VerViajero(ID_Viajero) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajero/Details?ID_Viajero=' + ID_Viajero);
    popupDiv(div, "", 25, 430);
    div.dialog('open');
}

function AsignarViajero(ID_Viajero) {
    var div = $("#LevantarPopup");
    div.empty();
    div.load('/Viajero/AsignarViajero?ID_Viajero=' + ID_Viajero);
    popupDiv(div, "", 25, 430);
    div.dialog('open');
}